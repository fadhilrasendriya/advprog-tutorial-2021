package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;

/**
 * This is just caesar cipher
 */

public class IsometricTransformation {
    private int[] key;

    public IsometricTransformation(int[] key){
        this.key = key;
    }

    /**
     * Constructor
     * @param strKey string a-z saja
     */
    public IsometricTransformation(String strKey){
        this(new int[strKey.length()]);
        for(int i = 0; i < key.length; i++){
            key[i] = (strKey.charAt(i) - 'a');
        }
    }

    public IsometricTransformation(){
        this("theshiningbeaconinabravenewworld");
    }

    public Spell encode(Spell spell){
        return process(spell, true);
    }

    public Spell decode(Spell spell){
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode){
        int selector = encode ? 1 : -1;
        String text = spell.getText();
        Codex codex = spell.getCodex();
        int codexSize = codex.getCharSize();
        char[] res = new char[text.length()];
        int n = res.length;
        for(int i = 0; i < n; i++){
            char oldChar = text.charAt(i);
            int charIndex = codex.getIndex(oldChar);
            int newCharIndex = (charIndex - key[i % key.length] * selector) % codexSize;
            newCharIndex = newCharIndex < 0 ? newCharIndex + codexSize : newCharIndex % codexSize;
            res[i] = codex.getChar(newCharIndex);
        }

        return new Spell(new String(res), codex);
    }
}
