package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;

public interface MenuFactory {
    Menu getMenu(String name, String menuName);
}
