package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class OrderServiceTest {

    OrderService orderService = new OrderServiceImpl();

    @Test
    public void testOrderDrinkIsWorking() {
        OrderDrink orderDrink = orderService.getDrink();
        orderService.orderADrink("WaterPistol");
        assertEquals("WaterPistol", orderDrink.getDrink());
    }

    @Test
    public void testOrderFoodIsWorking() {
        OrderFood orderFood = orderService.getFood();
        orderService.orderAFood("Fireball");
        assertEquals("Fireball", orderFood.getFood());
    }
}
