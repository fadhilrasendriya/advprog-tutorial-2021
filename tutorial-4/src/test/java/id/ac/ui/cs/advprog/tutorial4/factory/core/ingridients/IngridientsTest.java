package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.*;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.*;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.*;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.*;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class IngridientsTest {
    Flavor flavor;
    Meat meat;
    Noodle noodle;
    Topping topping;

    @Test
    public void testSaltyFlavor() {
        flavor = new Salty();
        assertNotNull(flavor.getDescription());
    }

    @Test
    public void testSpicyFlavor() {
        flavor = new Spicy();
        assertNotNull(flavor.getDescription());
    }

    @Test
    public void testSweetFlavor() {
        flavor = new Sweet();
        assertNotNull(flavor.getDescription());
    }

    @Test
    public void testUmamiFlavor() {
        flavor = new Umami();
        assertNotNull(flavor.getDescription());
    }


    @Test
    public void testBeefMeat() {
        meat = new Beef();
        assertNotNull(meat.getDescription());
    }

    @Test
    public void testChickenMeat() {
        meat = new Chicken();
        assertNotNull(meat.getDescription());
    }

    @Test
    public void testFishMeat() {
        meat = new Fish();
        assertNotNull(meat.getDescription());
    }

    @Test
    public void testPorkMeat() {
        meat = new Pork();
        assertNotNull(meat.getDescription());
    }


    @Test
    public void testRamenNoodle() {
        noodle = new Ramen();
        assertNotNull(noodle.getDescription());
    }

    @Test
    public void testShiratakiNoodle() {
        noodle = new Shirataki();
        assertNotNull(noodle.getDescription());
    }

    @Test
    public void testSobaNoodle() {
        noodle = new Soba();
        assertNotNull(noodle.getDescription());
    }

    @Test
    public void testUdonNoodle() {
        noodle = new Udon();
        assertNotNull(noodle.getDescription());
    }


    @Test
    public void testBoiledEggTopping() {
        topping = new BoiledEgg();
        assertNotNull(topping.getDescription());
    }

    @Test
    public void testCheeseTopping() {
        topping = new Cheese();
        assertNotNull(topping.getDescription());
    }

    @Test
    public void testFlowerTopping() {
        topping = new Flower();
        assertNotNull(topping.getDescription());
    }

    @Test
    public void testMushroomTopping() {
        topping = new Mushroom();
        assertNotNull(topping.getDescription());
    }


}
