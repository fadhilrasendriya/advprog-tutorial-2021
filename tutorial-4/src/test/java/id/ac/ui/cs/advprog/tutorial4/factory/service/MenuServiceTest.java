package id.ac.ui.cs.advprog.tutorial4.factory.service;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MenuFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class MenuServiceTest {



    @Mock
    MenuRepository repo = mock(MenuRepository.class);

    @Mock
    MenuFactory factory = mock(MenuFactory.class);

    @Mock
    Menu menu = mock(Menu.class);


    @InjectMocks
    MenuService menuService;

    @BeforeEach
    public void setUp() {
        menuService = new MenuServiceImpl(repo);
        when(factory.getMenu(anyString(), anyString())).thenReturn(menu);
        when(repo.getMenuFactory()).thenReturn(factory);
        when(repo.add(any(Menu.class))).thenReturn(menu);
    }

    @Test
    public void testMenuServiceGetMenuMethodCorrectlyImplemented() throws NullPointerException{
        List<Menu> list = new ArrayList<>();
        when(repo.getMenus()).thenReturn(list);
        menuService.getMenus();
        verify(repo, atLeastOnce()).getMenus();
    }

    @Test
    public void testMenuServiceCreateMenuMethodCorrectlyImplemented() throws NullPointerException{
        Menu returnedMenu = menuService.createMenu("Anna", "type");
        assertEquals(menu, returnedMenu);
        verify(repo, atLeastOnce()).getMenuFactory();

    }

}
