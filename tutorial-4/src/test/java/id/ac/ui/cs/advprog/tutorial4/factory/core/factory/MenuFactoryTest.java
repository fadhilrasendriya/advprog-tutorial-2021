package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;


import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class MenuFactoryTest {

    MenuFactory menuFactory = new MenuFactoryImpl();

    private static final String INUZUMA = "InuzumaRamen";
    private static final String LIYUAN = "LiyuanSoba";
    private static final String MONDO = "MondoUdon";
    private static final String SNEVNEZHA = "SnevnezhaShirataki";

    @Test
    public void testMenuFactoryGetMenuReturnsInuzumaRamenWithInuzumaRamenAsParameter() {
        Menu menu = menuFactory.getMenu("Ayaka", INUZUMA);
        assertTrue(menu instanceof InuzumaRamen);
    }

    @Test
    public void testMenuFactoryGetMenuReturnsLiyuanSobaWithLiyuanSobaAsParameter() {
        Menu menu = menuFactory.getMenu("Groza", LIYUAN);
        assertTrue(menu instanceof LiyuanSoba);
    }

    @Test
    public void testMenuFactoryGetMenuReturnsMondoUdonWithMondoUdonAsParameter() {
        Menu menu = menuFactory.getMenu("RPK", MONDO);
        assertTrue(menu instanceof MondoUdon);
    }

    @Test
    public void testMenuFactoryGetMenuReturnsSnevnezhaShiratakiWithSnevnezhaShiratakiAsParameter() {
        Menu menu = menuFactory.getMenu("Anna", SNEVNEZHA);
        assertTrue(menu instanceof SnevnezhaShirataki);
    }
}
