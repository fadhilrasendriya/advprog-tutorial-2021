package id.ac.ui.cs.advprog.tutorial4.factory.repository;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;


public class MenuRepositoryTest {
    private MenuRepository menuRepository = new MenuRepository();

    @Mock
    Menu menu = mock(Menu.class);

    @Test
    public void testMenuRepositoryAddMethodCorrectlyImplemented() throws NullPointerException {
        Menu returnedMenu = menuRepository.add(menu);
        assertEquals(menu, returnedMenu);
        assertEquals(menu, menuRepository.getMenus().get(0));
    }

    @Test
    public void testMenuRepositoryHasMenuFactory() throws NullPointerException {
        assertNotNull(menuRepository.getMenuFactory());
    }
}
