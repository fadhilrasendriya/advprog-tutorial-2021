package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class OrderFoodTest {
    OrderFood orderFood = OrderFood.getInstance();

    @Test
    public void testOrderFoodSetAndGet() {
        orderFood.setFood("Anna");
        assertEquals("Anna", orderFood.getFood());
    }
}
