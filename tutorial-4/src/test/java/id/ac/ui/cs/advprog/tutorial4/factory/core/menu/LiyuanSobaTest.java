package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class LiyuanSobaTest {

    private LiyuanSoba soba = new LiyuanSoba("Soba Mask");

    //Ingridients:
    //Noodle: Soba
    //Meat: Beef
    //Topping: Mushroom
    //Flavor: Sweet

    @Test
    public void testLiyuanSobaUseSobaNoodle() {
        Noodle noodle = soba.getNoodle();
        assertTrue(noodle instanceof Soba);
    }

    @Test
    public void testLiyuanSobaUseBeefMeat() {
        Meat meat = soba.getMeat();
        assertTrue(meat instanceof Beef);
    }

    @Test
    public void testLiyuanSobaUseMushroomTopping() {
        Topping topping = soba.getTopping();
        assertTrue(topping instanceof Mushroom);
    }

    @Test
    public void testLiyuanSobaUseSweetFlavor() {
        Flavor flavor = soba.getFlavor();
        assertTrue(flavor instanceof Sweet);
    }
}
