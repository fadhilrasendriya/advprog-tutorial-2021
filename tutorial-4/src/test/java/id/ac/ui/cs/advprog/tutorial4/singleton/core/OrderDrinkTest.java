package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class OrderDrinkTest {
    OrderDrink orderDrink = OrderDrink.getInstance();

    @Test
    public void testOrderDrinkSetAndGet() {
        orderDrink.setDrink("Anna");
        assertEquals("Anna", orderDrink.getDrink());
    }
}
