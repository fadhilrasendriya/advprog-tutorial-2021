package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {

    @Override
    public String attack() {
        return "Santoryu Ogi: Ichidai Sanzen Daisen Sekai!";
    }

    @Override
    public String getType() {
        return "Sword";
    }
}
