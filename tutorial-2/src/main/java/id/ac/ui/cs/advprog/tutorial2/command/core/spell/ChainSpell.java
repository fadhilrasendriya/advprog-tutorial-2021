package id.ac.ui.cs.advprog.tutorial2.command.core.spell;
import java.util.List;
import java.util.ListIterator;

public class ChainSpell implements Spell {

    private List<Spell> spells;

    public ChainSpell(List<Spell> spells) {
        this.spells = spells;
    }

    @Override
    public void cast() {
        for (Spell spell:
             spells) {
            if(doCast(spell)) {
                spell.cast();
            }
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }

    @Override
    public void undo() {
        ListIterator<Spell> listIterator = spells.listIterator(spells.size());
        while(listIterator.hasPrevious()) {
            Spell spell = listIterator.previous();
            if(doCast(spell)) {
                spell.cast();
            }
        }
    }

    private boolean doCast(Spell spell) {
        if(spell.spellName().contains("Seal")) {
           return false;
        } else if(spell.spellName().contains("Defense")) {
            return false;
        }
        return true;
    }

}
